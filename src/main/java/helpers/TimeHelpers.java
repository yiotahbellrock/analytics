package helpers;

import org.json.simple.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeHelpers {

    Date today = Calendar.getInstance().getTime();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd yyyy HH:mm:ss.SSS zzz");
    JSONObject generalData = new JSONObject();

    public long getTimeStamp() throws ParseException{
        String currentTime = simpleDateFormat.format(today);
        Date date = simpleDateFormat.parse(currentTime);
        return date.getTime();
    }

}
