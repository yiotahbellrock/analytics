package icaDemonstratorAnalytic;

import org.junit.Test;
import redis.clients.jedis.Jedis;

import java.util.Random;
import java.util.UUID;

public class ICADemonstrator {

    public static void main(String[] args) {

        try{
            ICARunner icaRunner = new ICARunner();
            icaRunner.runCalcs(args);
        }
        catch(Exception e){
            Jedis jedis = new Jedis(System.getenv("REDIS_ADDR"));
            jedis.set(args[0], e.getMessage());
        }
    }

    @Test
    public void testICADemonstrator(){
        Random randomNum = new Random(); //instance of random class
        String[] args = new String[5];
        args[0] = UUID.randomUUID().toString();
        args[1] = String.valueOf(randomNum.nextDouble());
        args[2] = String.valueOf(randomNum.nextDouble());
        args[3] = String.valueOf(randomNum.nextDouble());
        args[4] = String.valueOf(randomNum.nextDouble());
        main(args);
        System.out.println("UUID Written to redis: " + args[0]);
    }

}
