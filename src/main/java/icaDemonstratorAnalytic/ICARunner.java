package icaDemonstratorAnalytic;

import helpers.TimeHelpers;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisException;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ICARunner {

    private final static boolean RAND_GENERATOR = false;
    private final static boolean USE_LOW = false;
    private final static boolean USE_HIGH = false;
    TimeHelpers timeHelpers = new TimeHelpers();

    //Main code routines for projecting income:
    public void runCalcs(String[] args) throws ParseException {

        Date today = Calendar.getInstance().getTime();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM dd yyyy HH:mm:ss.SSS zzz");
        JSONObject generalData = new JSONObject();
        String uuid = "";
        long projdatetime = timeHelpers.getTimeStamp();

        //Variable used to retrieve the date on which the code was run:
        Calendar calcDate = Calendar.getInstance();
        int day = calcDate.get(Calendar.DAY_OF_MONTH);
        int month = calcDate.get(Calendar.MONTH);
        int year = calcDate.get(Calendar.YEAR);
        int noOfCases = 1;
        //Initial Income variables and settings; only apply where random number generators not being used:
        int levConvIncome = 2500;
        int rpiConvIncome = 3000;
        int icaMinInc = 2000;
        int icaMaxInc = 4000;
        int icaSelInc = 3500;
        double selRSR = 0.04;
        //Variables to hold declared RSRs and Inflation rate:
        double rpiRate = 0.03;
        double lowAnnouncedRSR = 0.03;
        double midAnnouncedRSR = 0.05;
        double highAnnouncedRSR = 0.07;
        //Variables for use in iterating through the years for income projection:
        double minInc = 0;
        double chosenInc = 0;
        double highInc = 0;
        double rpiIInc = 0;
        double loopedRSR = 0;
        int maxAdd = 0;
        //Variables for use in randomising income projection inputs:

        if(args[0] == null){
            System.out.println("Proper Usage is: java program filename");
            System.exit(0);
        }
        else{
            uuid = args[0];
            rpiRate = Double.parseDouble(args[1]);
            lowAnnouncedRSR = Double.parseDouble(args[2]);
            midAnnouncedRSR = Double.parseDouble(args[3]);
            highAnnouncedRSR = Double.parseDouble(args[4]);
        }

        for (int caseLoop = 1; caseLoop <= noOfCases; caseLoop++){
            if (RAND_GENERATOR){
                levConvIncome = randomInt((int) (Math.random()), 1000, 3500, 10000);
                rpiConvIncome = randomInt((int) (Math.random()), 1000, 3500, 10000);
                icaMinInc = randomInt((int) (Math.random()), 1000, 3500, 10000);
                maxAdd = randomInt((int) (Math.random()), 200, 900, 1000);
                icaMaxInc = icaMinInc + maxAdd;
                icaSelInc = randomInt((int) (Math.random()), (int) (icaMinInc), (int) (icaMaxInc), 10000);
                selRSR = randomInt((int) Math.random(), 1, 60 ,100);
                selRSR = selRSR / 1000;
                if (USE_LOW){
                    icaSelInc = icaMinInc;
                }
                if (USE_HIGH){
                    icaSelInc = icaMaxInc;
                }
            }

            generalData.put("projdatetime", projdatetime);
            generalData.put("Selected RSR", selRSR);
            generalData.put("Minimum Income", icaMinInc);
            generalData.put("Maximum Income", icaMaxInc);
            generalData.put("Chosen Income", icaSelInc);
            generalData.put("Low Announced SR", lowAnnouncedRSR);
            generalData.put("Mid Announced SR", midAnnouncedRSR);
            generalData.put("High Announced SR", highAnnouncedRSR);

            for (int rate = 1; rate <=4; rate ++){
                if (rate == 2){
                    loopedRSR = lowAnnouncedRSR;
                }
                else if (rate == 3){
                    loopedRSR = midAnnouncedRSR;
                }
                else {
                    loopedRSR = highAnnouncedRSR;
                }

                JSONArray incomeObject = new JSONArray();

                for (int projYear = 1; projYear <= 20; projYear ++) {
                    JSONObject yearlyProjection = new JSONObject();
                    JSONArray values = new JSONArray();
                    if (rate == 1){
                        rpiIInc = roundVal((rpiIInc*(1 + rpiRate)), -1);
                        if (projYear ==1){
                            rpiIInc = rpiConvIncome;
                        }
                        //yearlyProjection.put("yr" + projYear + "projlevinc", levConvIncome);
                        //yearlyProjection.put("yr" + projYear + "projrpiinc", rpiIInc);
                    }
                    else{
                        minInc = roundVal(minInc * ((1 + selRSR)*(1 + loopedRSR)), -1);
                        chosenInc = roundVal(chosenInc *((1 + selRSR) * (1 + loopedRSR)), -1);
                        highInc = roundVal(highInc * ((1 + selRSR)*(1 + loopedRSR)), -1);
                        if (projYear < 2){
                            minInc = icaMinInc;
                            chosenInc = icaSelInc;
                            highInc = icaMaxInc;
                        }
                        if(projYear == 20 && rate == 4){
                            generalData.put("yr" + projYear + "projmininc", minInc);
                            generalData.put("yr" + projYear + "projchoseninc", chosenInc);
                            generalData.put("yr" + projYear + "projhighinc", highInc);
                        }
                        //yearlyProjection.put("yr" + projYear + "projmininc", minInc);
                        //yearlyProjection.put("yr" + projYear + "projchoseninc", chosenInc);
                        //yearlyProjection.put("yr" + projYear + "projhighinc", highInc);
                    }
                    //incomeObject.add(yearlyProjection);
                }
                //if (rate == 1){generalData.put("Conventional Annuity Projection", incomeObject);}
                //else if (rate == 2){generalData.put("ICA with " + (int) (lowAnnouncedRSR * 100) + "% RSR", incomeObject);}
                //else if (rate == 3){generalData.put("ICA with " + (int) (midAnnouncedRSR * 100) + "% RSR", incomeObject);}
                //else {generalData.put("ICA with " + (int) (highAnnouncedRSR * 100) + "% RSR", incomeObject);}
            }
            try{
                Jedis jedis = new Jedis(System.getenv("REDIS_ADDR"));
                jedis.set(uuid, generalData.toJSONString());
            }
            catch(JedisException e){
                e.printStackTrace();
            }
        }
    }

    public int randomInt(int loopedInt, int lowVal, int highVal, int myMultiplier){
        do
            loopedInt = (int) (Math.random() * myMultiplier);
        while ((loopedInt >= highVal) || (loopedInt <= lowVal));
        return (int) loopedInt;
    }

    public double roundVal(double val, int roundScale){
        BigDecimal bd = new BigDecimal(Double.toString(val));
        bd = bd.setScale(roundScale, BigDecimal.ROUND_DOWN);
        return bd.doubleValue();
    }

}
