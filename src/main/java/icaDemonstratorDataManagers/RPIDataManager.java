package icaDemonstratorDataManagers;

import helpers.TimeHelpers;
import org.json.simple.JSONObject;
import org.junit.Test;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisException;

import java.text.ParseException;
import java.util.UUID;

public class RPIDataManager {

    public static void main(String[] args) throws ParseException {

        TimeHelpers timeHelpers = new TimeHelpers();
        JSONObject generalData = new JSONObject();
        String uuid = "";
        double rpiRate = 0.03;
        long timeStamp = timeHelpers.getTimeStamp();

        if(args[0] == null){
            System.out.println("Proper Usage is: java -jar program uuid");
            System.exit(0);
        }
        else{
            uuid = args[0];
            rpiRate = (Math.random() / 10);
        }

        generalData.put("timeStamp", timeStamp);
        generalData.put("rpi", rpiRate);

        try{
            Jedis jedis = new Jedis(System.getenv("REDIS_ADDR"));
            jedis.set(uuid, generalData.toJSONString());
        }
        catch(JedisException e){
            e.printStackTrace();
        }

    }

    @Test
    public void testRPIDataManager() throws ParseException {
        String[] args = new String[5];
        args[0] = UUID.randomUUID().toString();
        main(args);
        System.out.println("UUID Written to redis: " + args[0]);
    }
}
